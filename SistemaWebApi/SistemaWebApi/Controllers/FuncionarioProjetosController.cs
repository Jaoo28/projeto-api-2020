﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemaWebApi.Dados;
using SistemaWebApi.Models;

namespace SistemaWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FuncionarioProjetosController : ControllerBase
    {
        private readonly SistemaWebApiContext _context;

        public FuncionarioProjetosController(SistemaWebApiContext context)
        {
            _context = context;
        }

        // GET: api/FuncionarioProjetoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FuncionarioProjeto>>> GetFuncionarioProjeto()
        {
            return await _context.FuncionarioProjeto.ToListAsync();
        }

        // GET: api/FuncionarioProjetoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FuncionarioProjeto>> GetFuncionarioProjeto(int id)
        {
            var funcionarioProjeto = await _context.FuncionarioProjeto.FindAsync(id);

            if (funcionarioProjeto == null)
            {
                return NotFound();
            }

            return funcionarioProjeto;
        }

        // PUT: api/FuncionarioProjetoes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFuncionarioProjeto(int id, FuncionarioProjeto funcionarioProjeto)
        {
            if (id != funcionarioProjeto.FuncionarioId)
            {
                return BadRequest();
            }

            _context.Entry(funcionarioProjeto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FuncionarioProjetoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FuncionarioProjetoes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<FuncionarioProjeto>> PostFuncionarioProjeto(FuncionarioProjeto funcionarioProjeto)
        {
            _context.FuncionarioProjeto.Add(funcionarioProjeto);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FuncionarioProjetoExists(funcionarioProjeto.FuncionarioId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFuncionarioProjeto", new { id = funcionarioProjeto.FuncionarioId }, funcionarioProjeto);
        }

        // DELETE: api/FuncionarioProjetoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FuncionarioProjeto>> DeleteFuncionarioProjeto(int id)
        {
            var funcionarioProjeto = await _context.FuncionarioProjeto.FindAsync(id);
            if (funcionarioProjeto == null)
            {
                return NotFound();
            }

            _context.FuncionarioProjeto.Remove(funcionarioProjeto);
            await _context.SaveChangesAsync();

            return funcionarioProjeto;
        }

        private bool FuncionarioProjetoExists(int id)
        {
            return _context.FuncionarioProjeto.Any(e => e.FuncionarioId == id);
        }
    }
}
