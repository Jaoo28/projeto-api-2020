﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebApi.Models
{
    public class FuncionarioProjeto
    {
        public int FuncionarioId { get; set; }
        public int ProjetoId { get; set; }

        public Funcionario Funcionario { get; set; }
        public Projeto Projeto { get; set; }
    }
}
