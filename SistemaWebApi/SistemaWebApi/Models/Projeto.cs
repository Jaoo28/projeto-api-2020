﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebApi.Models
{
    public class Projeto
    {
        public int Id { get; set; }
        public string NomeProjeto { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime PrevisaoDeTermino { get; set; }

        public int IdSetorDoProjeto { get; set; } //vai guardar a qual setor esse projeto pertence
        public List<FuncionarioProjeto> FuncionarioProjeto { get; set; }
    }
}
