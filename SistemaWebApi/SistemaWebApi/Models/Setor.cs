﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaWebApi.Models
{
    public class Setor
    {
        public int Id { get; set; }
        public string NomeSetor { get; set; }
        public DateTime InicioServico { get; set; }
        public int IdSupervisor { get; set; }
        public List<Projeto> Projetos { get; set; }
       
    }
}
