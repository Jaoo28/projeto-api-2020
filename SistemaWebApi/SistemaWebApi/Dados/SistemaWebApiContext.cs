﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SistemaWebApi.Models;
using Microsoft.EntityFrameworkCore;

namespace SistemaWebApi.Dados
{
    public class SistemaWebApiContext : DbContext
    {
        public SistemaWebApiContext(DbContextOptions<SistemaWebApiContext> options) : base(options)
        {
        }

       protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FuncionarioProjeto>()
                .HasKey(ac => new { ac.FuncionarioId, ac.ProjetoId });
        }
      
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<Projeto> Projetos { get; set; }
        public DbSet<Setor> Setores { get; set; }
        public DbSet<SistemaWebApi.Models.FuncionarioProjeto> FuncionarioProjeto { get; set; }

    }
}
